import 'package:flutter/material.dart';

class HeroDialogPoint extends StatelessWidget {
  final String tag;
  final Widget child;

  const HeroDialogPoint({
    Key? key,
    required this.tag,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(32),
        child: Hero(
          tag: tag,
          child: Material(
            borderRadius: BorderRadius.circular(5),
            child: SingleChildScrollView(
              child: child,
            ),
          ),
        ),
      ),
    );
  }
}
