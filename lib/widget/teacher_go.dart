import 'package:flutter/material.dart';
import 'package:university/models/global_selection.dart';
import 'package:university/services/hive/type/teacher.dart';
import 'package:university/ui/page/teacher_profile.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'go_button.dart';

class TeacherGo extends StatelessWidget {
  final Teacher teacher;

  const TeacherGo({Key? key, required this.teacher}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GoButton(
      onTap: () {
        context.read(globalSelectionP).teacher = teacher;

        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => TeacherProfilePage(),
          ),
        );
      },
      title: '${teacher.name}',
    );
  }
}
