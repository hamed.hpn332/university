import 'package:flutter/material.dart';
import 'package:university/models/global_selection.dart';
import 'package:university/ui/page/day/current_day.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'go_button.dart';

class DayGo extends StatelessWidget {
  final int day;

  const DayGo({Key? key, required this.day}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GoButton(
      onTap: () {
        context.read(globalSelectionP).currentDay = day;

        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => CurrentDayPage()),
        );
      },
      title: 'day $day',
    );
  }
}
