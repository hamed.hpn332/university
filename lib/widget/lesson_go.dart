import 'package:flutter/material.dart';
import 'package:university/models/global_selection.dart';
import 'package:university/services/hive/type/lesson.dart';
import 'package:university/ui/page/lesson_profile.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'go_button.dart';

class LessonGo extends StatelessWidget {
  final Lesson lesson;

  const LessonGo({Key? key, required this.lesson}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GoButton(
      onTap: () {
        context.read(globalSelectionP).lesson = lesson;

        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => LessonProfilePage(),
          ),
        );
      },
      title: '${lesson.title}',
    );
  }
}
