import 'package:hive/hive.dart';
import 'package:hive_wrapper/hive_wrapper.dart';

import '../hive_wrapper.dart';
import 'teacher.dart';

part 'lesson.g.dart';

@HiveType(typeId: 0)
class Lesson extends HiveObjectWrapper {
  @HiveField(0)
  late String title;

  @HiveField(1)
  late int teacherId;

  @HiveField(2)
  late int day;

  @HiveField(3)
  late int startTime;

  @HiveField(4)
  late int endTime;

  Lesson({
    this.title = '',
    this.teacherId = 0,
    this.day = 0,
    this.startTime = 0,
    this.endTime = 0,
  });

  getField(name) {
    if (name == 'teacherId') {
      return teacherId;
    }
  }

  Teacher? get teacher => hasOne(hiveW.teachers, ownerKey: teacherId);

  double get time => endTime - startTime.toDouble();
}
