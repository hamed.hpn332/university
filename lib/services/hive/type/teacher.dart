import 'package:hive/hive.dart';
import 'package:hive_wrapper/hive_wrapper.dart';

import '../hive_wrapper.dart';
import 'lesson.dart';

part 'teacher.g.dart';

@HiveType(typeId: 1)
class Teacher extends HiveObjectWrapper {
  @HiveField(0)
  late String name;

  Iterable<Lesson> get lessons =>
      belongsTo(hiveW.lessons, getForeignKey: (e) => e.teacherId);

  Future<void> addclassByDay({
    required int day,
    required void Function(void Function(String, int, int)) adder,
  }) async {
    adder((title, startTime, endTime) {
      hiveW.lessons.add(
        title: title,
        day: day,
        teacherId: key,
        startTime: startTime,
        endTime: endTime,
      );
    });
  }

  Future<int> addClass(Lesson lesson) async {
    return await hiveW.lessons.box.add(lesson..teacherId = key);
  }
}
