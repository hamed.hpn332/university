import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:hive_wrapper/hive_wrapper.dart';
import 'package:university/services/hive/type/teacher.dart';

import 'type/lesson.dart';

final hiveW = HiveWrapper();

class HiveWrapper extends HostHiveWrapper {
  final boxs = {
    'lessons': BoxLessons(),
    'teachers': BoxTeachers(),
  };

  BoxLessons get lessons => boxs['lessons'] as BoxLessons;
  BoxTeachers get teachers => boxs['teachers'] as BoxTeachers;

  Future<void> loadHive() async {
    await Hive.initFlutter();

    Hive.registerAdapter(TeacherAdapter());
    Hive.registerAdapter(LessonAdapter());

    await lessons.load();
    await teachers.load();
  }
}

extension HW on HiveWrapper {}

class BoxLessons extends BoxWrapper<Lesson> {
  BoxLessons() : super('lesson');

  @override
  Future<void> initBox(Box<Lesson> box) async {}

  Future<int> add({
    required String title,
    required int day,
    required int teacherId,
    required int startTime,
    required int endTime,
  }) {
    return hiveW.lessons.box.add(
      Lesson(
        title: title,
        day: day,
        teacherId: teacherId,
        startTime: startTime,
        endTime: endTime,
      ),
    );
  }

  List<Lesson> getByDay(int day) {
    return where((element) => element.day == day).toList();
  }

  Lesson getById(int id) {
    return where((element) => element.key == id).first;
  }

  List<Lesson> allSorted() {
    return all.toList()..sort((a, b) => a.endTime.compareTo(b.startTime));
  }
}

class BoxTeachers extends BoxWrapper<Teacher> {
  BoxTeachers() : super('teacher');

  @override
  Future<void> initBox(Box<Teacher> box) async {}

  Future<Teacher> add(String name) async {
    final teacher = Teacher()..name = name;

    final teacherKey = await box.add(teacher);

    return box.get(teacherKey)!;
  }

  Teacher getById(int id) {
    return where((element) => element.key == id).first;
  }
}
