import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:university/services/hive/type/lesson.dart';
import 'package:university/services/hive/type/teacher.dart';

final globalSelectionP = Provider((ref) => GlobalSelection());

class GlobalSelection {
  late Teacher teacher;
  late Lesson lesson;
  late int currentDay;
}
