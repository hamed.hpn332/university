import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:university/services/hive/hive_wrapper.dart';

import 'app.dart';
import 'services/hive/type/lesson.dart';

void main() async {
  await hiveW.loadHive();

  await loadBaseData();

  runApp(ProviderScope(child: App()));
}

Future<void> loadBaseData() async {
  await hiveW.teachers.box.clear();
  await hiveW.lessons.box.clear();

  await hiveW.teachers.add('قلی پور')
    ..addclassByDay(
      day: 5,
      adder: (add) {
        add('مباحث ویژه 1', 1000, 1200);
        add('مباحث ویژه 2', 1200, 1400);
        add('تجارت الکترونیکی', 1600, 1800);
      },
    );

  await hiveW.teachers.add('ساسان نیا زهرا')
    ..addClass(
      Lesson(
        title: 'سیگنالها و سیستم ها',
        day: 2,
        startTime: 1600,
        endTime: 1800,
      ),
    );

  await hiveW.teachers.add('نور محمدی محمد')
    ..addClass(
      Lesson(
        title: 'ریزپردازنده و زبان اسمبلی',
        day: 2,
        startTime: 1800,
        endTime: 2000,
      ),
    );

  await hiveW.teachers.add('خادمی داوود')
    ..addClass(
      Lesson(
        title: 'آیین زندگی (علوم پایه و فنی مهندسی)',
        day: 3,
        startTime: 1800,
        endTime: 2000,
      ),
    );
}
