import 'package:flutter/material.dart';
import 'package:university/models/global_selection.dart';
import 'package:university/services/hive/type/lesson.dart';
import 'package:university/widget/day_go.dart';
import 'package:university/widget/lesson_go.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class TeacherProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final teacher = context.read(globalSelectionP).teacher;

    return Scaffold(
      body: Center(
        child: Column(
          children: [
            Row(
              children: [
                BackButton(),
                Text('${teacher.name}'),
              ],
            ),
            Divider(),
            Expanded(
              child: GridView(
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 200),
                children:
                    teacher.lessons.map((e) => lessonItem(context, e)).toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget lessonItem(BuildContext context, Lesson lesson) {
    return Card(
      margin: const EdgeInsets.all(8),
      child: Column(
        children: [
          LessonGo(lesson: lesson),
          Divider(),
          DayGo(day: lesson.day),
        ],
      ),
    );
  }
}
