import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:university/models/global_selection.dart';
import 'package:university/widget/day_go.dart';
import 'package:university/widget/teacher_go.dart';

class LessonProfilePage extends HookWidget {
  @override
  Widget build(BuildContext context) {
    final lesson = context.read(globalSelectionP).lesson;

    return Scaffold(
      backgroundColor: Colors.grey,
      body: Center(
        child: Container(
          width: 600,
          height: 500,
          child: Material(
            elevation: 6,
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Row(
                      children: [
                        Wrap(
                          children: [
                            BackButton(),
                            Text(
                              '${lesson.title}',
                              style: TextStyle(fontSize: 36),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Divider(),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 30),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Text('مدرس'),
                              SizedBox(width: 50),
                              TeacherGo(teacher: lesson.teacher!),
                            ],
                          ),
                          Row(
                            children: [
                              Text('روز'),
                              SizedBox(width: 50),
                              DayGo(day: lesson.day),
                            ],
                          ),
                          Row(
                            children: [
                              Text('زمان'),
                              SizedBox(width: 50),
                              Text('${lesson.startTime} -> ${lesson.endTime}'),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
