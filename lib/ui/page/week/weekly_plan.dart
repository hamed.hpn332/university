import 'package:flutter/material.dart';

import 'chart.dart';

class WeeklyPlanPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      body: Center(
        child: Container(
          color: Colors.white,
          width: 1000,
          padding: const EdgeInsets.symmetric(vertical: 16),
          child: SingleChildScrollView(
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Chart(),
            ),
          ),
        ),
      ),
    );
  }

  Widget bar(BuildContext context) {
    return Material(
      elevation: 6,
      child: Row(
        children: [
          // BackButton(),
          Text('week'),
        ],
      ),
    );
  }
}
