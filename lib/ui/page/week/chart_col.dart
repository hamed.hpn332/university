import 'package:flutter/material.dart';
import 'package:university/services/hive/type/lesson.dart';
import 'package:university/widget/day_go.dart';
import 'package:university/widget/lesson_go.dart';

import 'chart.dart';

class ChartCol extends StatelessWidget {
  final int dayIndex;
  final Iterable<Lesson> lessonOfDay;

  ChartCol(this.dayIndex, this.lessonOfDay);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.amber[(dayIndex + 1) * 100],
      constraints: BoxConstraints(
        minWidth: minColWidth,
      ),
      child: Column(
        children: [
          // title
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: DayGo(day: dayIndex),
            ),
          ),

          // column
          createItem()
        ],
      ),
    );
  }

  Widget createItem() {
    if (lessonOfDay.isEmpty) {
      return empty(Text('0000\n2400'), 2400);
    }

    final rowLinesData = compileData(lessonOfDay);

    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: buildRow(rowLinesData),
    );
  }

  List<List<Lesson>> compileData(Iterable<Lesson> lessonOfDay) {
    final timeWeight = <int, int>{};
    final lessonsRow = <List<Lesson>>[];

    for (final lessons in lessonOfDay) {
      if (timeWeight.containsKey(lessons.startTime)) {
        timeWeight[lessons.startTime] = timeWeight[lessons.startTime]! + 1;
      } else {
        timeWeight[lessons.startTime] = 0;
      }

      if (lessonsRow.length == timeWeight[lessons.startTime]!) {
        lessonsRow.add([lessons]);
      } else {
        lessonsRow[timeWeight[lessons.startTime]!].add(lessons);
      }
    }

    return lessonsRow;
  }

  List<Widget> buildRow(List<List<Lesson>> rowLinesData) {
    final allRow = <Widget>[];

    for (final rowLine in rowLinesData) {
      final lessonsInRow = <Widget>[];

      /// first
      if (rowLine.first.startTime > 0) {
        lessonsInRow.add(
          empty(
            Text('0000\n${rowLine.first.startTime}'),
            rowLine.first.startTime.toDouble(),
          ),
        );
      }

      lessonsInRow.addAll(generateInBetweenLesson(rowLine));

      /// last
      if (rowLine.last.endTime < 2400) {
        lessonsInRow.add(
          empty(
            Text('${rowLine.last.endTime}\n2400'),
            2400.0 - rowLine.last.endTime,
          ),
        );
      }

      allRow.add(
        Column(children: lessonsInRow),
      );
    }

    return allRow;
  }

  // some time empty time exist
  // need to detect
  List<Widget> generateInBetweenLesson(List<Lesson> rowLine) {
    final lessonsRow = <Widget>[];

    for (var i = 0; i < rowLine.length; i++) {
      final lesson = rowLine[i];

      lessonsRow.add(
        item(
          LessonGo(lesson: lesson),
          lesson.time,
        ),
      );

      if (i + 1 < rowLine.length) {
        final nextLesson = rowLine[i + 1];

        if (lesson.endTime != nextLesson.startTime) {
          lessonsRow.add(
            empty(
              Text('${lesson.endTime}\n${nextLesson.startTime}'),
              nextLesson.startTime - lesson.endTime.toDouble(),
            ),
          );
        }
      }
    }

    return lessonsRow;
  }

  Widget item(Widget child, [double height = 200, Color? color]) {
    return Container(
      constraints: BoxConstraints(
        maxWidth: minColWidth,
        maxHeight: height / 2.5,
      ),
      child: Card(
        color: color,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: child,
          ),
        ),
      ),
    );
  }

  Widget empty(Widget child, double height) {
    return Container(
      constraints: BoxConstraints(
        maxWidth: minColWidth,
        maxHeight: height / 2.5,
      ),
      child: Card(
        elevation: .1,
        color: Colors.white.withOpacity(0.1),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: child,
          ),
        ),
      ),
    );
  }
}
