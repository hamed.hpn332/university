import 'package:flutter/material.dart';
import 'package:university/services/hive/hive_wrapper.dart';
import 'package:university/services/hive/type/lesson.dart';

import 'chart_col.dart';

final kWidth = 1000.0;
final kCol = 7;
double get minColWidth => kWidth / kCol;
// final kMaxHeight = 2400.0 / 3;

class Chart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final lessons = hiveW.lessons.allSorted();

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: createDayColumns(lessons),
    );
  }

  List<Widget> createDayColumns(List<Lesson> lessons) {
    return List.generate(
      kCol,
      (dayIndex) {
        final lessonOfDay = lessons.where((element) => element.day == dayIndex);

        return ChartCol(dayIndex, lessonOfDay);
      },
    );
  }
}
