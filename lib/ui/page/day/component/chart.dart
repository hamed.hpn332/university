import 'package:flutter/material.dart';
import 'package:university/services/hive/type/lesson.dart';
import 'package:university/widget/lesson_go.dart';
import 'package:university/widget/teacher_go.dart';

class Chart extends StatelessWidget {
  final List<ChartDataTime> data;

  const Chart({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          ...makeList(context),
        ],
      ),
    );
  }

  List<Widget> makeList(BuildContext context) {
    final list = <Widget>[];

    for (var i = 0; i < data.length; i++) {
      final item = data[i];

      ChartDataTime? nextItem;
      if (i + 1 < data.length) nextItem = data[i + 1];

      if (i == 0 && item.start > 0) list.add(emptyTime());

      list.add(timeRow(item, context));
      list.add(
        Container(
          width: double.infinity,
          height: 2,
          color: Colors.grey.shade400,
        ),
      );

      if ((i == data.length - 1) && item.end < 2400) {
        list.add(emptyTime());
      } else if (item.end != nextItem?.start) {
        list.add(emptyTime());
      }
    }

    return list;
  }

  Widget emptyTime() {
    return Container(
      width: double.infinity,
      height: 50,
      color: Colors.grey.shade300,
    );
  }

  Widget timeRow(ChartDataTime dataByTime, BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Row(
          children: [
            Text('${dataByTime.start}\n${dataByTime.end}'),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ...dataByTime.row
                      .map((lesson) => item(lesson, context))
                      .toList(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget item(Lesson lesson, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          LessonGo(lesson: lesson),
          Divider(),
          TeacherGo(teacher: lesson.teacher!),
        ],
      ),
    );
  }
}

class ChartDataTime {
  final int start;
  final int end;
  final List<Lesson> row;

  ChartDataTime({required this.start, required this.end, required this.row});
}

class ChartLessonData {
  final Lesson lesson;

  ChartLessonData({required this.lesson});
}
