import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:university/models/global_selection.dart';
import 'package:university/services/hive/hive_wrapper.dart';

import 'component/chart.dart';

class CurrentDayPage extends HookWidget {
  @override
  Widget build(BuildContext context) {
    final day = context.read(globalSelectionP).currentDay;

    return Scaffold(
      body: Column(
        children: [
          bar(context, day),
          Expanded(
            child: Chart(
              data: [
                ...createData(day),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget bar(BuildContext context, int day) {
    return Material(
      elevation: 6,
      child: Row(
        children: [
          BackButton(),
          Text('$day day'),
        ],
      ),
    );
  }

  List<ChartDataTime> createData(int day) {
    final dayLessons = hiveW.lessons.getByDay(day);

    final timeDataList = <ChartDataTime>[];

    dayLessons.forEach(
      (lesson) {
        final list =
            timeDataList.where((element) => element.start == lesson.startTime);

        if (list.isEmpty) {
          timeDataList.add(
            ChartDataTime(
              start: lesson.startTime,
              end: lesson.endTime,
              row: [lesson],
            ),
          );
        } else {
          list.first.row.add(lesson);
        }
      },
    );

    timeDataList.sort((a, b) => a.end.compareTo(b.start));

    return timeDataList;
  }
}
